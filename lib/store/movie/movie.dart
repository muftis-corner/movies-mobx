import 'package:mobx/mobx.dart';

part 'movie.g.dart';

class MovieData extends _MovieData with _$MovieData {
  MovieData(
      int id, String title, String director, String summary, List<String> tags)
      : super(id, title, director, summary, tags);

  static MovieData get empty => MovieData(0, '', '', '', []);
}

abstract class _MovieData with Store {
  @observable
  int id;
  @observable
  String title;
  @observable
  String director;
  @observable
  String summary;
  @observable
  List<String?> tags = [];

  _MovieData(
    this.id,
    this.title,
    this.director,
    this.summary,
    this.tags,
  );
}
