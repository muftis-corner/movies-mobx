// import 'package:movies_mobx/model/movie.model.dart';
import 'package:movies_mobx/store/movie/movie.dart';
import 'package:mobx/mobx.dart';
import 'dart:math';

part 'movieList.g.dart';

class MovieList = _MovieList with _$MovieList;

abstract class _MovieList with Store {
  @observable
  ObservableList<MovieData> movies = ObservableList();

  @action
  void addMovie(MovieData value) {
    movies.add(value);
  }

  @action
  void removeMovie(MovieData value) {
    movies.removeWhere((element) => value == element);
  }

  @action
  void updateMovie(MovieData value) {
    int id = movies.indexWhere((element) => element.id == value.id);
    movies[id] = value;
  }

  @action
  int generateNewId() {
    if (movies.length < 1) {
      return 0;
    }
    return movies.map((element) => element.id).reduce(max) + 1;
  }
}
