// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../page/MovieEditPage.dart' as _i5;
import '../page/MovieNewPage.dart' as _i4;
import '../page/MoviePage.dart' as _i3;
import '../store/movie/movie.dart' as _i7;
import '../store/movie/movieList.dart' as _i6;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    MovieRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i3.MoviePage();
        }),
    MovieNewRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<MovieNewRouteArgs>();
          return _i4.MovieNewPage(key: args.key, movieList: args.movieList);
        }),
    MovieEditRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<MovieEditRouteArgs>();
          return _i5.MovieEditPage(args.movieList, args.movieData,
              key: args.key);
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(MovieRoute.name, path: '/'),
        _i1.RouteConfig(MovieNewRoute.name, path: '/movie-new-page'),
        _i1.RouteConfig(MovieEditRoute.name, path: '/movie-edit-page')
      ];
}

class MovieRoute extends _i1.PageRouteInfo {
  const MovieRoute() : super(name, path: '/');

  static const String name = 'MovieRoute';
}

class MovieNewRoute extends _i1.PageRouteInfo<MovieNewRouteArgs> {
  MovieNewRoute({_i2.Key? key, required _i6.MovieList movieList})
      : super(name,
            path: '/movie-new-page',
            args: MovieNewRouteArgs(key: key, movieList: movieList));

  static const String name = 'MovieNewRoute';
}

class MovieNewRouteArgs {
  const MovieNewRouteArgs({this.key, required this.movieList});

  final _i2.Key? key;

  final _i6.MovieList movieList;
}

class MovieEditRoute extends _i1.PageRouteInfo<MovieEditRouteArgs> {
  MovieEditRoute(
      {required _i6.MovieList movieList,
      required _i7.MovieData movieData,
      _i2.Key? key})
      : super(name,
            path: '/movie-edit-page',
            args: MovieEditRouteArgs(
                movieList: movieList, movieData: movieData, key: key));

  static const String name = 'MovieEditRoute';
}

class MovieEditRouteArgs {
  const MovieEditRouteArgs(
      {required this.movieList, required this.movieData, this.key});

  final _i6.MovieList movieList;

  final _i7.MovieData movieData;

  final _i2.Key? key;
}
