import 'package:auto_route/annotations.dart';
import 'package:movies_mobx/page/MovieEditPage.dart';
import 'package:movies_mobx/page/MovieNewPage.dart';
import 'package:movies_mobx/page/MoviePage.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: MoviePage, initial: true),
    AutoRoute(page: MovieNewPage),
    AutoRoute(page: MovieEditPage),
  ],
)
class $AppRouter {}
