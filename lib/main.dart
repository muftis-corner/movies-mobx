import 'package:flutter/material.dart';
import 'package:movies_mobx/route/router.gr.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _router = AppRouter();
  final _theme = ThemeData(
    primarySwatch: Colors.red,
    primaryColorBrightness: Brightness.dark,
    appBarTheme: AppBarTheme(
      brightness: Brightness.dark,
      backgroundColor: Colors.black,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: _theme,
      debugShowCheckedModeBanner: false,
      routeInformationParser: _router.defaultRouteParser(),
      routerDelegate: _router.delegate(),
    );
  }
}
