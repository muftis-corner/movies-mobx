import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'package:movies_mobx/components/button.dart';
import 'package:movies_mobx/components/movieForm.dart';
// import 'package:movies_mobx/model/movie.model.dart';
import 'package:movies_mobx/store/movie/movie.dart';
import 'package:movies_mobx/store/movie/movieList.dart';

class MovieEditPage extends StatelessWidget {
  final MovieList movieList;
  final MovieData movieData;
  const MovieEditPage(
    this.movieList,
    this.movieData, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    MovieData _selectedMovie = movieData;

    AutoRouter.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Update  Movie'),
        actions: [
          CButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                movieList.updateMovie(movieData);
                context.popRoute();
              }
            },
            title: "Update",
          ),
          CButton(
              onPressed: () {
                movieList.removeMovie(_selectedMovie);
                context.popRoute();
              },
              title: "Delete")
        ],
      ),
      body: MovieForm(
        formKey: _formKey,
        movie: movieData,
      ),
    );
  }
}
