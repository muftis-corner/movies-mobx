import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import 'package:movies_mobx/components/button.dart';
import 'package:movies_mobx/components/movieForm.dart';
// import 'package:movies_mobx/model/movie.model.dart';
import 'package:movies_mobx/store/movie/movie.dart';
import 'package:movies_mobx/store/movie/movieList.dart';

class MovieNewPage extends StatelessWidget {
  final MovieList movieList;
  const MovieNewPage({
    Key? key,
    required this.movieList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    MovieData newMovie = MovieData.empty;
    newMovie.id = movieList.generateNewId();

    AutoRouter.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('New  Movie'),
        actions: [
          CButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                movieList.addMovie(newMovie);
                context.popRoute();
              }
            },
            title: "Save",
          )
        ],
      ),
      body: MovieForm(
        formKey: _formKey,
        movie: newMovie,
      ),
    );
  }
}
