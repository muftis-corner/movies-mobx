import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movies_mobx/components/button.dart';
import 'package:movies_mobx/components/movieItem.dart';
import 'package:movies_mobx/page/MovieNewPage.dart';
import 'package:movies_mobx/route/router.gr.dart';
import 'package:movies_mobx/store/movie/movieList.dart';

class MoviePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AutoRouter.of(context);

    MovieList movieList = MovieList();
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: Colors.black,
          title: Text('Movies'),
          centerTitle: false,
          actions: [
            CButton(
              onPressed: () async {
                // var _router = AutoRouter.of(context);
                // MovieData result = await context.pushRoute(MovieNewRoute()) as MovieData;
                // AutoRouter.of(context).push(MovieNewRoute(movieList: movieList));
                // _router.push(MovieNewRoute(movieList: movieList));
                // context.push(MovieNewRoute(movieList: movieList));

                // context.pushRoute();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MovieNewPage(
                              movieList: movieList,
                            )));
              },
              title: "New",
            )
          ],
        ),
        body: Observer(
            builder: (context) => ListView.builder(
                itemCount: movieList.movies.length,
                itemBuilder: (_, index) =>
                    MovieItem(movieList.movies[index], movieList))));
  }
}
