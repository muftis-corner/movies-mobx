import 'package:flutter/material.dart';
import 'package:movies_mobx/components/textField.dart';
// import 'package:movies_mobx/model/movie.model.dart';
import 'package:movies_mobx/store/movie/movie.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class MovieForm extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final MovieData movie;
  const MovieForm({
    Key? key,
    required this.formKey,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: ListView(
        children: [
          CTextField(
            initialValue: movie.title,
            validator: _validator,
            placeholder: "Movie Title",
            onChanged: (value) {
              movie.title = value;
            },
          ),
          CTextField(
            initialValue: movie.director,
            validator: _validator,
            placeholder: "Director",
            onChanged: (value) {
              movie.director = value;
            },
          ),
          CTextField(
            initialValue: movie.summary,
            validator: _validator,
            placeholder: "Summary",
            onChanged: (value) {
              movie.summary = value;
            },
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: MultiSelectDialogField<String?>(
              listType: MultiSelectListType.LIST,
              initialValue: movie.tags,
              title: Text('Tags'),
              buttonText: Text('Tags'),
              items: ['Horor', 'Comedy', "Action"]
                  .map((e) => MultiSelectItem(e, e))
                  .toList(),
              onConfirm: (value) {
                movie.tags = value;
              },
              searchable: true,
            ),
          )
        ],
      ),
    );
  }

  String? _validator(String? value) {
    if (value == null || value.isEmpty) return 'Please Enter some text';
    return null;
  }
}
