import 'package:flutter/material.dart';

class CButton extends StatelessWidget {
  final String title;
  final Function() onPressed;
  const CButton({Key? key, this.title = "", required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: this.onPressed,
          child: Text(this.title),
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.all(8)),
          )),
    );
  }
}
