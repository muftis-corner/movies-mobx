import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
// import 'package:movies_mobx/model/movie.model.dart';
import 'package:movies_mobx/page/MovieEditPage.dart';
import 'package:movies_mobx/store/movie/movie.dart';
import 'package:movies_mobx/store/movie/movieList.dart';

class MovieItem extends StatelessWidget {
  final MovieData movie;
  final MovieList movieList;
  const MovieItem(this.movie, this.movieList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MovieEditPage(movieList, movie)));
          },
          title: Observer(builder: (_) => Text(movie.title)),
          subtitle: Observer(builder: (_) => Text(movie.director)),
        ),
        Divider()
      ],
    );
  }
}
