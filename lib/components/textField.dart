import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CTextField extends StatefulWidget {
  final String initialValue;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onSubmitted;
  final FormFieldValidator<String>? validator;
  final String? placeholder;

  CTextField({
    Key? key,
    this.initialValue = "",
    this.onChanged,
    this.onEditingComplete,
    this.onSubmitted,
    this.placeholder,
    this.validator,
  }) : super(key: key);

  @override
  _CTextFieldState createState() => _CTextFieldState();
}

class _CTextFieldState extends State<CTextField> {
  late TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController(text: widget.initialValue);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _controller,
        validator: widget.validator,
        onChanged: widget.onChanged,
        onEditingComplete: widget.onEditingComplete,
        decoration: InputDecoration(hintText: widget.placeholder),
      ),
    );
  }
}
